package com.verizon.vnf.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.verizon.vnf.model.WorkFlowView;
import com.verizon.vnf.model.WorkFlowViewObject;
import com.verizon.vnf.repository.FormDataRepository;
import com.verizon.vnf.services.PackageHandlerImpl;
import com.verizon.vnf.services.WorkflowHandlerImpl;
import com.verizon.vnf.util.Util;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/vnfPackage")
public class PackageController {
	@Autowired
	FormDataRepository repositoryImplClass;

	@Autowired
    private SimpMessagingTemplate template;
	
	@Autowired
	Util util;
	
	@Autowired
	PackageHandlerImpl packageHandler;
	
	@Autowired
	WorkflowHandlerImpl workflowHandler;

	//Upload File 
	@RequestMapping(value = "{vnfID}/UploadFile", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API is used to Upload a given File", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> UploadCompletePackage(@RequestPart MultipartFile uploadFile, @RequestPart String checksumValue ,@PathVariable String vnfID){	
		Map<String,String> message = new HashMap<String,String>();
		try {
			System.out.println("inside VNF controller ----- ");
			String response = packageHandler.uploadFile(util.convert(uploadFile),checksumValue,vnfID);
			message.put("type", "sucess");
			message.put("message", "sucessfully uploaded");
			message.put("id", response);
			
			WorkFlowView workFlowViewUpload = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectUpload = new WorkFlowViewObject();
			workFlowViewObjectUpload.setStatus("completed");
			if(workflowHandler.retriveID(vnfID) != null)
			{
				workFlowViewUpload=workflowHandler.retriveID(vnfID);
			}
			workFlowViewUpload.setUpload(workFlowViewObjectUpload);
			repositoryImplClass.save(workFlowViewUpload);
			
			this.template.convertAndSend("/jenkins/"+vnfID, workFlowViewObjectUpload);
			
			/*
			Thread.sleep(15000);
			
			WorkFlowView workFlowViewPV = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectPV = new WorkFlowViewObject();
			workFlowViewObjectPV.setStatus("completed");
			workFlowViewPV=retriveID(id);
			workFlowViewPV.setPackage_validation(workFlowViewObjectPV);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewPV);
			
			Thread.sleep(10000);
			
			WorkFlowView workFlowViewSecurity = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectSecurity = new WorkFlowViewObject();
			workFlowViewObjectSecurity.setStatus("completed");
			workFlowViewSecurity=retriveID(id);
			workFlowViewSecurity.setSecurity(workFlowViewObjectSecurity);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewSecurity);
			
			Thread.sleep(12000);
			
			WorkFlowView workFlowViewInstantiation = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectInstantiation = new WorkFlowViewObject();
			workFlowViewObjectInstantiation.setStatus("completed");
			workFlowViewInstantiation=retriveID(id);
			workFlowViewInstantiation.setInstantiation(workFlowViewObjectInstantiation);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewInstantiation);
			
			Thread.sleep(20000);
			
			WorkFlowView workFlowViewTU = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectTU = new WorkFlowViewObject();
			workFlowViewObjectTU.setStatus("completed");
			workFlowViewTU=retriveID(id);
			workFlowViewTU.setTest_updates(workFlowViewObjectTU);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewTU);
			
			Thread.sleep(15000);
			
			WorkFlowView workFlowViewCert = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectCert = new WorkFlowViewObject();
			workFlowViewObjectCert.setStatus("completed");
			workFlowViewCert=retriveID(id);
			workFlowViewCert.setCertification(workFlowViewObjectCert);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewCert);
			
			Thread.sleep(5000);
			
			WorkFlowView workFlowViewAVU = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectAVU = new WorkFlowViewObject();
			workFlowViewObjectAVU.setStatus("completed");
			workFlowViewAVU=retriveID(id);
			workFlowViewAVU.setArtifactory_version_update(workFlowViewObjectAVU);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewAVU);
			*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return message;		
	}
	
			
}
