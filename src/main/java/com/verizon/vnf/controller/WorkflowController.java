package com.verizon.vnf.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.verizon.vnf.model.ValidationStatusBody;
import com.verizon.vnf.model.WorkFlowView;
import com.verizon.vnf.model.WorkFlowViewObject;
import com.verizon.vnf.repository.FormDataRepository;
import com.verizon.vnf.services.PackageHandlerImpl;
import com.verizon.vnf.services.WorkflowHandlerImpl;
import com.verizon.vnf.util.Util;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/vnfWorkFlow")
public class WorkflowController {

	@Autowired
	FormDataRepository repositoryImplClass;
	
	@Autowired
	Util util;
	
	@Autowired
    private SimpMessagingTemplate template;
	
	@Autowired
	WorkflowHandlerImpl workflowHandler;
	
	@Autowired
	PackageHandlerImpl packageHandler;

    private static final String IMAGE_PATTERN =
                "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";


	//initialize
	@RequestMapping(value = "{vnfID}/initialize", method = RequestMethod.PUT, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf initialize", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Object initializeWorkflow(@PathVariable String vnfID){
	
		WorkFlowView workFlowView = new WorkFlowView();
		WorkFlowViewObject workFlowViewObject = new WorkFlowViewObject();
		workFlowViewObject.setStatus("not-started");
		
		workFlowView.setUpload(workFlowViewObject);
		workFlowView.setPackage_validation(workFlowViewObject);
		workFlowView.setSecurity(workFlowViewObject);
		workFlowView.setInstantiation(workFlowViewObject);
		workFlowView.setTest_updates(workFlowViewObject);
		workFlowView.setCertification(workFlowViewObject);
		workFlowView.setArtifactory_version_update(workFlowViewObject);
		workFlowView.setId(vnfID);
		//repositoryImplClass.save(id,workFlowView);
		repositoryImplClass.save(workFlowView);
	
		return workFlowView;
	}
	
    @MessageMapping("/hello")
	@SendTo("/jenkins/{vnfID}")
    public WorkFlowView updateEvents(WorkFlowView workflowView) throws Exception {
		return workflowView;	
    }


	@RequestMapping(value = "{vnfID}/updateStatus", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "This API used to provide the vnf update", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> update(@PathVariable String vnfID,@RequestBody ValidationStatusBody valStatusbody){
		WorkFlowView workFlowView = new WorkFlowView();
		WorkFlowViewObject workFlowViewObject = new WorkFlowViewObject();
		workFlowViewObject.setStatus(valStatusbody.getStatus());
		Map<String,String> message = new HashMap<String,String>();
		if(workflowHandler.retriveID(vnfID) != null){
			workFlowView = workflowHandler.retriveID(vnfID);
			}			
			 switch (valStatusbody.getPhase()) {
	         case "upload":
	        	 workFlowView.setUpload(workFlowViewObject);
	             break;
	         case "package_validation":
	        	 workFlowView.setPackage_validation(workFlowViewObject);
	             break;
	         case "security":
	        	 workFlowView.setSecurity(workFlowViewObject);
	             break;
	         case "instantiation":
	        	 workFlowView.setInstantiation(workFlowViewObject);
	             break;
	         case "test_updates":
	        	 workFlowView.setTest_updates(workFlowViewObject);
	             break;
	         case "certification":
	        	 workFlowView.setCertification(workFlowViewObject);
	             break;
	         case "artifactory_version_update":
	        	 workFlowView.setArtifactory_version_update(workFlowViewObject);
	             break;
		     }
			 repositoryImplClass.save(workFlowView);
		this.template.convertAndSend("/jenkins/"+vnfID, workFlowView);

		message.put("type", "sucess");
		message.put("message", "Data Updated sucessfully!");
		return message;
		
	}
	
	//retrieve date
	@RequestMapping(value = "{vnfID}/retrieve", method = RequestMethod.GET, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf retrive", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Object retrieve(@PathVariable String vnfID){
		System.out.println("Id: "+vnfID);
		WorkFlowView object = (WorkFlowView) repositoryImplClass.findOne(vnfID);		
		return object;	
	}
	

	//Upload File 
	@RequestMapping(value = "{vnfID}/UploadFile", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API is used to Upload a given File", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> UploadFile(@RequestPart MultipartFile uploadFile, @RequestPart String checksumValue ,@PathVariable String vnfID){	
		Map<String,String> message = new HashMap<String,String>();
		try {
			System.out.println("inside VNF controller ----- ");
			String response = packageHandler.uploadFile(util.convert(uploadFile),checksumValue,vnfID);
			message.put("type", "sucess");
			message.put("message", "sucessfully uploaded");
			message.put("id", response);
			
			WorkFlowView workFlowViewUpload = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectUpload = new WorkFlowViewObject();
			workFlowViewObjectUpload.setStatus("completed");
			if(workflowHandler.retriveID(vnfID) != null)
			{
				workFlowViewUpload=workflowHandler.retriveID(vnfID);
			}
			workFlowViewUpload.setUpload(workFlowViewObjectUpload);
			repositoryImplClass.save(workFlowViewUpload);
			
			this.template.convertAndSend("/jenkins/"+vnfID, workFlowViewObjectUpload);
			
			/*
			Thread.sleep(15000);
			
			WorkFlowView workFlowViewPV = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectPV = new WorkFlowViewObject();
			workFlowViewObjectPV.setStatus("completed");
			workFlowViewPV=retriveID(id);
			workFlowViewPV.setPackage_validation(workFlowViewObjectPV);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewPV);
			
			Thread.sleep(10000);
			
			WorkFlowView workFlowViewSecurity = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectSecurity = new WorkFlowViewObject();
			workFlowViewObjectSecurity.setStatus("completed");
			workFlowViewSecurity=retriveID(id);
			workFlowViewSecurity.setSecurity(workFlowViewObjectSecurity);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewSecurity);
			
			Thread.sleep(12000);
			
			WorkFlowView workFlowViewInstantiation = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectInstantiation = new WorkFlowViewObject();
			workFlowViewObjectInstantiation.setStatus("completed");
			workFlowViewInstantiation=retriveID(id);
			workFlowViewInstantiation.setInstantiation(workFlowViewObjectInstantiation);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewInstantiation);
			
			Thread.sleep(20000);
			
			WorkFlowView workFlowViewTU = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectTU = new WorkFlowViewObject();
			workFlowViewObjectTU.setStatus("completed");
			workFlowViewTU=retriveID(id);
			workFlowViewTU.setTest_updates(workFlowViewObjectTU);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewTU);
			
			Thread.sleep(15000);
			
			WorkFlowView workFlowViewCert = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectCert = new WorkFlowViewObject();
			workFlowViewObjectCert.setStatus("completed");
			workFlowViewCert=retriveID(id);
			workFlowViewCert.setCertification(workFlowViewObjectCert);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewCert);
			
			Thread.sleep(5000);
			
			WorkFlowView workFlowViewAVU = new WorkFlowView();
			WorkFlowViewObject workFlowViewObjectAVU = new WorkFlowViewObject();
			workFlowViewObjectAVU.setStatus("completed");
			workFlowViewAVU=retriveID(id);
			workFlowViewAVU.setArtifactory_version_update(workFlowViewObjectAVU);
			repositoryImplClass.save(WORKFLOW, id, workFlowViewAVU);
			*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return message;		
	}
	
	//ValidatePackage
	@RequestMapping(value = "/ValidatePackage", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API is used to Upload a given File", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> ValidatePackage(@RequestPart String vnfId){
		System.out.println("11111111");
		String saveDirectory = "/home/invlab09/testupload/Karan/a.txt";
		System.out.println("22222222222");
		Map<String,String> message = new HashMap<String,String>();
		File file = new File(saveDirectory);
		boolean exists =file.exists();
		System.out.println("existsssssssssssss"+exists);
		//String response = packageHandler.validatePackage();
		message.put("type", "sucess");
		message.put("message", "sucessfully validated");
		message.put("id", "Done");
		return message;		
	}
	
	@RequestMapping(value = "/crunchifySave", method = RequestMethod.POST)
	@ApiOperation(value = "This API is used to Upload a given File", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
    public String crunchifySave(@RequestPart  MultipartFile uploadFile,@RequestPart String vnfId) throws IllegalStateException, IOException {
		System.out.println("Entered11111111");
		String saveDirectory = createDirectoryIfNeeded("/home/invlab09/testupload/"+vnfId+"/");
        //String saveDirectory = "/home/invlab09/testupload/";
		System.out.println("22222222222");
		//uploadFile = ((CrunchifyFileUpload) uploadFile).getFiles();
        List<String> fileNames = new ArrayList<String>();
 
        if(null != uploadFile && uploadFile.getSize() > 0) {
        	System.out.println("444444444444444");
           // for (MultipartFile multipartFile : uploadFile) {
                String fileName = uploadFile.getOriginalFilename();
                if(fileName.contains("script")) {
                	System.out.println("33333333333333");
                    // Handle file content - multipartFile.getInputStream()
                	String newDirectory = createDirectoryIfNeeded(saveDirectory+"scripts/");
                	uploadFile.transferTo(new File(newDirectory+fileName));
                    fileNames.add(fileName);
                } else if(Pattern.compile(IMAGE_PATTERN).matcher(fileName.toLowerCase()).matches()) {
                	System.out.println("7777777");
                    // Handle file content - multipartFile.getInputStream()
                	String newDirectory = createDirectoryIfNeeded(saveDirectory+"icons/");
                	uploadFile.transferTo(new File(newDirectory+fileName));
                    fileNames.add(fileName);
                } else if(fileName.endsWith(".qcow2")) {
                	System.out.println("99999");
                    // Handle file content - multipartFile.getInputStream()
                	String newDirectory = createDirectoryIfNeeded(saveDirectory+"images/");
                	uploadFile.transferTo(new File(newDirectory+fileName));
                    fileNames.add(fileName);
                }
            }
        //}
       // map.addAttribute("files", fileNames);
        return "uploadfilesuccess";
    }
	
	private String createDirectoryIfNeeded(String directoryName)
	{
		File theDir = new File(directoryName); 
		if (!theDir.exists()){
			theDir.mkdirs();
		} else {
			System.out.println("Exists");
		}
	return directoryName;
	}
	
}