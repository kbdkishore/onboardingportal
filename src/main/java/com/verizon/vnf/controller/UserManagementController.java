package com.verizon.vnf.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.verizon.vnf.repository.UserManagementRepository;
import com.verizon.vnf.util.Util;
import com.verizon.vnf.model.AddUser;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/vnfUser")
public class UserManagementController {
	private static final int SESSION_TIME = 3600;
	@Autowired
	UserManagementRepository repositoryImplClass;
	
	@Autowired
	Util util;
	
	@RequestMapping(value = "/create-user", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "Creates the bridge with controller specific information ", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Created"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
		public @ResponseBody Map<String,String> addUser(@RequestBody AddUser user){
		Map<String,String> message = new HashMap<String,String>();	
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); 
		user.setCreateDate(dateFormat.format(date));
		repositoryImplClass.save(user);
		message.put("type", "sucess");
		message.put("message", "Data save sucessfully!");
		System.out.println("save User Done!");
		return message;
	}
	
	//retrieve date
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf retrive", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Object retrieve(@RequestParam String userid, HttpServletRequest request, HttpSession session){		
		System.out.println("Id: "+userid);
		Object object = repositoryImplClass.findOne(userid);
			request.getSession();
			String uid = session.getId(); 
			session.setAttribute("uid", uid);
			session.setMaxInactiveInterval(SESSION_TIME);
		return object;	
	}	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf retrive", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public void Logout(HttpServletRequest request, HttpSession session){
		try{
			session.invalidate();
		} catch(Exception e){
			System.out.println("Unable to close the session");
		}
	}	
	
}
