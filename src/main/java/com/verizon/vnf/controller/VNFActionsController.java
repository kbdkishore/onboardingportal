package com.verizon.vnf.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.verizon.vnf.model.PopulateNsd;
import com.verizon.vnf.model.Vnfd;
import com.verizon.vnf.repository.RepositoryImplClass;
import com.verizon.vnf.services.VnfActionsHandlerImpl;
import com.verizon.vnf.util.Util;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/vnf")
public class VNFActionsController {
	
	@Autowired
	RepositoryImplClass repositoryImplClass;
	
	@Autowired
	Util util;

	@Autowired
	VnfActionsHandlerImpl VnfActionsHandler;
	
	
	//retrieve Orch reg ID
	@RequestMapping(value = "retrieveOrchRegId", method = RequestMethod.GET, produces = "application/json")	
	@ApiOperation(value = "This API used to retreive Orchestrator reg ID", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retreived Orchestrator Registration ID"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Orchestrator does not exist"), })
	public Object getOssRegistrationId(@PathVariable String orchName){		
		System.out.println("orchName: "+orchName);
		String orchRegID = VnfActionsHandler.getOssRegistrationId(orchName);
		return orchRegID;	
	}
	
	
	@RequestMapping(value = "/createVnfd", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> CreateVnfd(@RequestBody Vnfd vnfd){	
		Map<String,String> message = new HashMap<String,String>();
		
		VnfActionsHandler.createVnfd("C:\\Users\\TEST\\Desktop\\vnfOnboarding\\openimscore-packages-master", vnfd.getVnfdName(), vnfd.getVendor(), vnfd.getVersion(), vnfd.getType(), vnfd.getEndpoint(), vnfd.getVmImage(), vnfd.getVim(), vnfd.getScaleInOut(), vnfd.getFloatingIp(), vnfd.getFlavor());
		
		return message;
		
	}
	
	@RequestMapping(value = "{vnfName}/uploadVnfd", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> UploadVnfd(@PathVariable String vnfName,@RequestPart MultipartFile uploadFile){	
		Map<String,String> message = new HashMap<String,String>();
		//"C:\\Users\\TEST\\Desktop\\vnfOnboarding\\openimscore-packages-master\\scscf"
		try {
			String response = VnfActionsHandler.uploadVnfdPackage(util.convert(uploadFile), vnfName);
			message.put("type", "sucess");
			message.put("message", "sucessfully uploaded");
			message.put("id", response);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return message;		
	}
	
	@RequestMapping(value = "{vnfName}/getVnfStatus", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> getVnfStatus(@PathVariable String vnfName){	
		Map<String,String> message = new HashMap<String,String>();
		//"C:\\Users\\TEST\\Desktop\\vnfOnboarding\\openimscore-packages-master\\scscf"
		String status = VnfActionsHandler.getVnfStatus(vnfName);
		message.put("type", "sucess");
		message.put("message", "sucessfully get Status");
		message.put("status", status);
		return message;		
	}
	
	@RequestMapping(value = "{nsdName}/activateVnf", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> activateVnf(@PathVariable String nsdName){
		Map<String,String> message = new HashMap<String,String>();		
		VnfActionsHandler.activateVNF(nsdName);
		message.put("type", "sucess");
		message.put("message", "sucessfully activate Vnf");
		return message;
		
	}
	
	@RequestMapping(value = "/createNsd", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> createNsd(@RequestBody PopulateNsd populateNsd){
		
		Map<String,String> message = new HashMap<String,String>();		
		message = VnfActionsHandler.populateNsd(populateNsd.getVnfdList(),"C:\\Users\\TEST\\Desktop\\vnfOnboarding\\openimscore-packages-master\\descriptors\\tutorial-ims-NSR");		
		return message;		
	}
	
	@RequestMapping(value = "/uploadNsd", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> uploadNsd(@RequestPart MultipartFile uploadFile) throws ParseException{
		
		Map<String,String> message = new HashMap<String,String>();	
		//util.uploadNsd(uploadFile);
		message.put("type", "sucess");
		message.put("message", "sucessfully upload NSD");		
		return message;		
	}	
	

/*	@RequestMapping(value = "{title}/get", method = RequestMethod.GET)	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Vnf get(@PathVariable String title){
		Vnf vnf = new Vnf();
		vnf.setCompanyname("");
		vnf.setHighleveldes("");
		vnf.setNetworkservice("");
		vnf.setVnfproductname("");
		CompanyTechnicalContact companyTechnicalContact = new CompanyTechnicalContact();
		companyTechnicalContact.setEmail("");
		companyTechnicalContact.setPhone("");
		vnf.setCompanytechnicalcontact(companyTechnicalContact);
		repositoryImplClass.save(VNF, title, vnf);
		return vnf;
	}
	*/
	
}
		
		