package com.verizon.vnf.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.verizon.vnf.model.FormData;
import com.verizon.vnf.repository.FormDataSaving;
import com.verizon.vnf.repository.RepositoryImplClass;
import com.verizon.vnf.util.Util;
@RestController
@RequestMapping("/vnfForm")
public class FormDataController {
	private static final String VNF = "VNF";
	@Autowired
	FormDataSaving repositoryImplClass;
	
	@Autowired
	Util util;
	@RequestMapping(value = "{userId}/{vnfID}/saveFormData", method = RequestMethod.POST, consumes = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> save(@PathVariable String userId, @PathVariable String vnfID, @RequestBody FormData formData){
		
		Map<String,String> message = new HashMap<String,String>();		
		/*Object object = repositoryImplClass.get(VNF, id);
		if(object !=null){
			message.put("type", "failure");
			message.put("message", "Already available!");
			return message;
		}*/
		formData.setUserId(userId);
		formData.setId(vnfID);
		repositoryImplClass.save(formData);
		message.put("type", "sucess");
		message.put("message", "Data save sucessfully!");
		return message;
	/*	message.put("testing", "add");
		message.put("testing1", "add1");
		message.put("testing2", "add2");
		message.put("testing3", "add3");
		message.put("testing4", "add4");
		
		Map<String,String> message1 = new HashMap<String,String>();
		message1.put("testing2", "add2");
		message1.put("testing3", "add3");
		message1.put("testing4", "add4");
		
		Map<String,String> message2 = new HashMap<String,String>();
		message2.put("testing2", "add2");
		message2.put("testing3", "add3");
		message2.put("testing4", "add4");
		
		Map<String,Map<String,String>> finalc = new HashMap<String,Map<String,String>>();
		finalc.put("1", message);
		finalc.put("2", message1);
		finalc.put("3", message2);
		
		formData.setFormData(finalc);	*/ 
		
		
	}
	
	@RequestMapping(value = "{userId}/{vnfID}/getFormData", method = RequestMethod.GET, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Object get(@PathVariable String userId, @PathVariable String vnfID){		
		System.out.println("userId: "+userId);
		Object object = repositoryImplClass.findByUserIdAndId(userId, vnfID);		
		return object;	
	}	
	
	@RequestMapping(value = "{userId}/getAllPackage", method = RequestMethod.GET, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Object getAll(@PathVariable String userId){		
		Map<String,Object> listMap = new LinkedHashMap<String,Object>();
		Object object = repositoryImplClass.findAllByUserId(userId);
		
		/*List<Object> object = repositoryImplClass.find(userId);
		Set<String> keys = repositoryImplClass.getAllKeys(userId);
		Iterator<Object> obj = object.iterator();
		Iterator<String> key = keys.iterator();
		while(obj.hasNext() && key.hasNext()){
			listMap.put(key.next(), obj.next());
		}*/
		return object;	
	}
	
	@RequestMapping(value = "{vnfID}/deleteForm", method = RequestMethod.DELETE, produces = "application/json")	
	@ApiOperation(value = "This API used to provide the vnf information", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful get all the data"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> deleteForm(@PathVariable String vnfID){		
		Map<String,String> message = new HashMap<String,String>();		
		repositoryImplClass.delete(vnfID);
		message.put("type", "sucess");
		message.put("message", "Form deleted sucessfully!");
		return message;
	}
}
