package com.verizon.vnf.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class Util {

	private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private Cipher cipher;
    byte[] arrayBytes;
    SecretKey key;

    public static File convert(MultipartFile file) throws IOException
	{    
	    File convFile = new File(file.getOriginalFilename());
	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(file.getBytes());
	    fos.close(); 
	    return convFile;
	}
	
	
	private static void saveToFile(InputStream inStream, String target)	throws IOException {
		System.out.println("target::111:"+target);
		OutputStream out = null;
		int read = 0;
		byte[] bytes = new byte[1024];
		out = new FileOutputStream(new File(target));
		while ((read = inStream.read(bytes)) != -1) {
			out.write(bytes, 0, read);
		}
		System.out.println("target::222:"+target);
		out.flush();
		out.close();
		
	}

	
	public void ValidateZip(String filePath) {
		try {
			ArrayList<String> set = new ArrayList<String>();
			set.add("ziptest/");
			//set.add("ziptest/test/test.txt");
			//set.add("ziptest/test/");
			set.add("alter_varchar_clob.sql");
			set.add("runtime_config.sql");
			String filename = "forZipTest";
			byte[] buf = new byte[1024];
			ZipInputStream zipinputstream = null;
			ZipEntry zipentry;
			zipinputstream = new ZipInputStream(new FileInputStream(filePath));
			ArrayList<String> zipNames = new ArrayList<String>();
			ArrayList<String> entryNames = new ArrayList<String>();
//			entryNames.add("")
			while ((zipentry = zipinputstream.getNextEntry()) != null) {
				String entryName = zipentry.getName();
				if (entryName.equals(filename + "/"))
					continue;
				entryNames.add(entryName);
				String s0 = entryName.substring(entryName.indexOf("/") + 1);

				if (s0.indexOf("/") > -1) {
					s0 = s0.substring(0, s0.indexOf("/"));
					if (!zipNames.contains(s0))
						zipNames.add(s0);
				}
			}
			System.out.println("zipNames:" + zipNames);
			System.out.println("entryNames: " + entryNames);
			if (!validate(filename, set, zipNames, entryNames))
				System.out.println("Zip is bad");
			else
				System.out.println("Zip is good");
			zipinputstream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public boolean validate(String filename, ArrayList<String> set, ArrayList<String> zipNames,
			ArrayList<String> entryNames) {
		boolean b = true;
		for (String s : zipNames) {
			for (String s1 : set) {
				String s2 = s1.replaceAll("fileName", filename);
				String s3 = s2.replaceAll("zipName", s);
				System.out.println("s3: " + s3);
				if (!entryNames.contains(s3)) {
					System.out.println("file does not contain " + s3);
					return false;
				}
				entryNames.remove(s3);
			}
		}
		
		if (entryNames.size() > 0) {
			/*if (entryNames.size() != set.size()){
				System.out.println("Size is different");
				return false;
			}*/
			HashMap<String, String> map = new HashMap<String, String>();
		    for (String str : set) {
		        map.put(str, str);
		    }
		    for (String str : entryNames) {
		        if ( ! map.containsKey(str) ) {
		            return false;
		        }
		    }
		}
		return b;
	}

	public String encrypt(String unencryptedString) {
        String encryptedString = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedString;
    }
	
	public String decrypt(String encryptedString) {
        String decryptedText=null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }
}
