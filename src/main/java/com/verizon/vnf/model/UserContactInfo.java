package com.verizon.vnf.model;

import java.io.Serializable;

public class UserContactInfo  implements Serializable{

	/**
	 * CompanyTechnicalContact
	 */
	
	private static final long serialVersionUID = 1L;
	private String address;
	private String email;
	private String phone;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

}
