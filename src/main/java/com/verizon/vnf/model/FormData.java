package com.verizon.vnf.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "FormData")
public class FormData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId;
	
	private String id;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	private Map<String,Object> formData = new LinkedHashMap<String,Object>();

	public Map<String,Object> getFormData() {
		return formData;
	}

	public void setFormData(Map<String,Object> formData) {
		this.formData = formData;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
