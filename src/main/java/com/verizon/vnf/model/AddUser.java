package com.verizon.vnf.model;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "USER")
public class AddUser implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private String username;
	private String createDate;
	private String actions;
	private boolean admin=false;
	private boolean vendor=false;
	private boolean owner=false;
	private ArrayList<UserInfo> userInfo = new ArrayList<UserInfo>();
	private ArrayList<UserContactInfo> userContactInfo = new ArrayList<UserContactInfo>();
	
	@Id
	private String userid;
	
	public String getUserid() {
		return userid;
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public boolean isOwner() {
		return owner;
	}
	public void setOwner(boolean owner) {
		this.owner = owner;
	}
	public String getActions() {
		return actions;
	}
	public void setActions(String actions) {
		this.actions = actions;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public boolean isVendor() {
		return vendor;
	}
	public void setVendor(boolean vendor) {
		this.vendor = vendor;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public ArrayList<UserContactInfo> getUserContactInfo() {
		return userContactInfo;
	}
	public void setUserContactInfo(ArrayList<UserContactInfo> userContactInfo) {
		this.userContactInfo = userContactInfo;
	}
	public ArrayList<UserInfo> getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(ArrayList<UserInfo> userInfo) {
		this.userInfo = userInfo;
	}
}
