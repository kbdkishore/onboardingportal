package com.verizon.vnf.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.verizon.vnf.model.FormData;

public interface FormDataSaving extends MongoRepository<FormData, String>{
		
		//FormData findFirstById(String domain);
	FormData findAllById(String userId);
	
	//FormData findByUserIdAndId(String userId, String Id);
	@Query("{ 'userId' : ?0"
            + " , 'id' : ?1 }"
            + "")
	FormData findByUserIdAndId(String userId, String id);

	//Long delete(String userId, String id);

	List<FormData> findAllByUserId(String userId);

	/*FormData findAll(String userId, String id);*/

	       
}
