package com.verizon.vnf.repository;




import org.springframework.data.mongodb.repository.MongoRepository;

import com.verizon.vnf.model.WorkFlowView;

public interface FormDataRepository extends MongoRepository<WorkFlowView, String>{
	
	//FormData findFirstById(String domain);
	WorkFlowView findFirstById(String domain);

       
}
